package draw

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"math/rand"
	"time"
)

type Draw struct {
	Date     time.Time
	Min, Max int
	Quantity int
	Results  []int
}

func abs(x int) int {
	if x >= 0 {
		return x
	}
	return -x
}

func random(min, max int) int {
	return rand.Intn(max+abs(min)) - abs(min)
}

func makeRandomRange(min, max, quantity int) []int {
	randomRange := make([]int, quantity)
	for i := 0; i < quantity; i++ {
		randomRange[i] = random(min, max)
	}
	return randomRange
}

func New(min int, max int, quantity int) Draw {
	randomRange := makeRandomRange(min, max, quantity)
	return Draw{time.Now(), min, max, quantity, randomRange}
}

func LoadDraws(filename string) (draws map[string][]Draw, err error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &draws)
	if err != nil {
		return nil, err
	}
	return draws, nil
}

func SaveDraws(draws map[string][]Draw, filename string) bool {
	jsonData, err := json.Marshal(draws)
	if err != nil {
		return false
	}
	// fmt.Println(string(jsonData))
	err = ioutil.WriteFile(filename, jsonData, 0644)
	if err != nil {
		return false
	}
	return true
}
