package main

import (
	"github.com/kgabis/numbers/draw"
	// "fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"
)

const drawsPath = "draws.json"

type readOperation struct {
	key      string
	response chan []draw.Draw
}

type writeOperation struct {
	key      string
	value    draw.Draw
	response chan bool
}

var templates = template.Must(template.ParseFiles("index.html", "view.html"))
var draws map[string][]draw.Draw
var reads chan *readOperation = make(chan *readOperation)
var writes chan *writeOperation = make(chan *writeOperation)

func renderTemplate(w http.ResponseWriter, tmpl string, context interface{}) {
	err := templates.ExecuteTemplate(w, tmpl+".html", context)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func manageDraws() {
	for {
		select {
		case read := <-reads:
			read.response <- draws[read.key]
		case write := <-writes:
			drawsByAuthor := draws[write.key]
			if drawsByAuthor == nil {
				drawsByAuthor = make([]draw.Draw, 0)
			}
			drawsByAuthor = append(drawsByAuthor, write.value)
			draws[write.key] = drawsByAuthor
			draw.SaveDraws(draws, drawsPath)
			write.response <- true
		}
	}
}

func updateDraws(author string, newDraw draw.Draw) {
	write := &writeOperation{
		key:      author,
		value:    newDraw,
		response: make(chan bool)}
	writes <- write
	<-write.response
}

func getDraws(author string) []draw.Draw {
	read := &readOperation{
		key:      author,
		response: make(chan []draw.Draw)}
	reads <- read
	drawsByAuthor := <-read.response
	return drawsByAuthor
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	author := r.URL.Query().Get("author")
	drawsByAuthor := getDraws(author)
	// fmt.Fprintf(w, "%v", drawsByAuthor)
	info := struct {
		Author string
		Draws  []draw.Draw
	}{
		author,
		drawsByAuthor,
	}
	renderTemplate(w, "view", info)
}

func drawHandler(w http.ResponseWriter, r *http.Request) {
	author := r.FormValue("Author")
	min, err1 := strconv.Atoi(r.FormValue("Min"))
	max, err2 := strconv.Atoi(r.FormValue("Max"))
	quantity, err3 := strconv.Atoi(r.FormValue("Quantity"))
	if err1 != nil || err2 != nil || err3 != nil {
		http.Error(w, "Error!", http.StatusInternalServerError)
		return
	}
	newDraw := draw.New(min, max, quantity)
	updateDraws(author, newDraw)
	http.Redirect(w, r, "/view?author="+author, http.StatusFound)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "index", nil)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("New request: %s", r.URL.RequestURI())
		fn(w, r)
	}
}

func main() {
	// go func() {
	// 	time.Sleep(60 * time.Second)
	// 	panic("blad")
	// }()
	var err error
	draws, err = draw.LoadDraws(drawsPath)
	if err != nil {
		draws = make(map[string][]draw.Draw)
	}
	go manageDraws()
	http.HandleFunc("/view", makeHandler(viewHandler))
	http.HandleFunc("/draw", makeHandler(drawHandler))
	http.HandleFunc("/", makeHandler(indexHandler))
	listenAddr := ":" + os.Getenv("PORT")
	log.Printf("Listening at http://%s", listenAddr)
	err = http.ListenAndServe(listenAddr, nil)
	if err != nil {
		log.Fatal(err)
	}
}
